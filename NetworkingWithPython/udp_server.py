import socket

def start_server(host='127.0.0.1', port=65432):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind((host, port))
        print(f"Server started on {host}:{port}")

        while True:
            data, addr = s.recvfrom(1024)  # Buffer size is 1024 bytes
            if not data:
                break
            print(f"Received from {addr}: {data.decode()}")
            reply = input("Server: ")
            s.sendto(reply.encode(), addr)

if __name__ == "__main__":
    start_server()
